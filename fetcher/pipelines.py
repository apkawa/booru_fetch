# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html
from cStringIO import StringIO
import os
import Image
import hashlib

from scrapy.contrib.pipeline.images import ImagesPipeline, FSImagesStore
from scrapy.utils.misc import md5sum

from scrapy.conf import settings


IMAGES_STORE_FORMAT_KEY = 'IMAGES_STORE_FORMAT'

IMAGES_STORE_FORMAT = settings.get(IMAGES_STORE_FORMAT_KEY, '{tag}/{image_name}')


class FetcherImagePipeline(ImagesPipeline):

    def process_item(self, item, spider):
        self._item = item
        spider._item = item
        return super(FetcherImagePipeline, self).process_item(item, spider)

    def image_key(self, url):
        return os.path.join(".tmp", hashlib.sha1(url).hexdigest() + os.path.splitext(url)[1])

    def get_images_store_format(self):
        image_store_format = (self.spiderinfo.spider.override_images_store_format or IMAGES_STORE_FORMAT)
        # import pudb; pudb.set_trace()
        return settings.overrides.get(IMAGES_STORE_FORMAT_KEY, image_store_format)

    def _build_image_key(self, tag, image_name, image_size=None):
        image_store_format = self.get_images_store_format()
        context = {
            'tag': tag,
            'image_size': image_size,
            'image_name': image_name,
        }
        return image_store_format.format(**context)

    def image_downloaded(self, response, request, info):
        checksum = None
        for key, image, buf in self.get_images(response, request, info):
            if checksum is None:
                buf.seek(0)
                checksum = md5sum(buf)
            self.store.persist_image(key, image, buf, info)
            source = self.store._get_filesystem_path(key)

            image_size = 'x'.join(map(str, image.size))
            for tag in self._item['tags']:
                new_key = self._build_image_key(
                    tag=tag,
                    image_name=os.path.split(key)[1],
                    image_size=image_size,
                )
                new_path = self.store._get_filesystem_path(new_key)
                if os.path.exists(new_path):
                    continue

                self.store._mkdir(os.path.dirname(new_path))
                os.link(source, new_path)

            if source != self._build_image_key(
                    tag = self._item['tags'][0],
                    image_name=os.path.split(key)[1],
                    image_size=image_size):
                os.remove(source)
        return checksum

    def get_images(self, response, request, info):
        key = self.image_key(request.url)
        buf = StringIO(response.body)
        orig_image = Image.open(buf)
        yield key, orig_image, buf





