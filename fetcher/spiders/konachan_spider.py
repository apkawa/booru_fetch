from __future__ import absolute_import

import os
from scrapy.http import Request
from scrapy.selector import HtmlXPathSelector
from scrapy.conf import settings

from scrapy.shell import inspect_response

from ..items import FetcherItem
from . import Spider


class FetcherSpider(Spider):
    start_urls = [
        "http://konachan.com/post"
        ]

    override_images_store_format = '{tag}/{image_size}/{image_name}'

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        for a in hxs.select("//li[contains(@class, 'creator-id-')]"):
            post_id = a.select('@id').extract()[0].lstrip('p')
            image_url = a.select("a[starts-with(@class, 'directlink')]/@href").extract()[0]
            if image_url.startswith("http://konachan.com/jpeg/"):
                image_url = os.path.splitext(
                        image_url.replace("http://konachan.com/jpeg/", "http://konachan.com/image/")
                    )[0] + '.png'

            t = a.select("div/a[@class='thumb']/img/@alt").extract()[0]
            tags = t.split('Tags: ', 1)[-1].split('User: ', 1)[0].strip().split(' ')

            yield FetcherItem(
                image_urls=[image_url],
                tags=tags,
                post_id=post_id,
            )


        next_url = hxs.select("//div[@id='paginator']//a[@rel='next']/@href").extract()
        if next_url:
            next_url = self.get_base_url() + next_url[0]
            yield Request(next_url, callback=self.parse)
