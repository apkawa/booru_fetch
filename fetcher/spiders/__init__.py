# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.
import urllib
from scrapy.spider import BaseSpider
import urlparse


class SpiderMeta(type):
    def __new__(mcls, name, bases, attrs):
        if 'start_urls' in attrs:
            attrs['name'] = urlparse.urlparse(attrs['start_urls'][0]).netloc.split('.', 1)[0]
        return super(SpiderMeta, mcls).__new__(mcls, name, bases, attrs)

class Spider(BaseSpider):
    __metaclass__ = SpiderMeta

    tag = None

    override_images_store_format = None

    allowed_domains = []

    def __init__(self, tag=None, *args, **kwargs):
        super(Spider, self).__init__(*args, **kwargs)
        parsed_url = urlparse.urlsplit(self.start_urls[0])

        self.allowed_domains = [parsed_url.netloc]
        self.tag = tag

        query = urlparse.parse_qs(parsed_url.query)
        query.update({'tags': tag})
        self.start_urls = [urlparse.urlunparse([parsed_url.scheme, parsed_url.netloc,
                                                parsed_url.path, '', urllib.urlencode(query), ''])]


    def get_base_url(self):
        parsed_url = urlparse.urlsplit(self.start_urls[0])
        base_url = urlparse.urlunparse([parsed_url.scheme, parsed_url.netloc, '', '', '', ''])
        return base_url
