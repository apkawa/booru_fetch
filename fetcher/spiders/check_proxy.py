# -*- coding: utf-8 -*-
from scrapy.spider import BaseSpider
from scrapy.http import Request

class FetcherSpider(BaseSpider):
    name = "check_proxy"

    tag = None


    start_urls = [
        "http://checkip.dyndns.org/",
    ]

    def parse(self, response):
        print response.body
        yield Request(self.start_urls[0], callback=self.parse)

