from __future__ import absolute_import

import os
from scrapy.http import Request
from scrapy.selector import HtmlXPathSelector

from scrapy.shell import inspect_response

from ..items import FetcherItem
from . import Spider

class FetcherSpider(Spider):
    start_urls = [
        "http://danbooru.donmai.us/posts?utf8=%E2%9C%93"
        ]

    def parse(self, response):
        """
        http://danbooru.donmai.us/ssd/data/preview/a82d30af265f79b90145719da08095a4.jpg
        http://danbooru.donmai.us/data/a82d30af265f79b90145719da08095a4.jpg
        """
        hxs = HtmlXPathSelector(response)
        for a in hxs.select("//div[@id='posts']/article"):
            post_id = a.select('@data-id').extract()[0]
            thumb_url = a.select('a/img/@src').extract()[0]
            image_url = "http://danbooru.donmai.us/data/{}".format(*self._parse_thumb_url(thumb_url))
            tags = a.select('@data-tags').extract()[0].strip().split(' ')

            yield FetcherItem(
                image_urls=[image_url],
                tags=tags,
                post_id=post_id
            )


        next_url = hxs.select("//div[@class='paginator']//a[@rel='next']/@href").extract()
        if next_url:
            next_url = self.get_base_url() + next_url[0]
            yield Request(next_url, callback=self.parse)

    def _parse_thumb_url(self, thumb_url):
        return [os.path.split(thumb_url)[1]]

