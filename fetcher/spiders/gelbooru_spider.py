from __future__ import absolute_import
import cgi

import re
import urlparse
from scrapy.http import Request
from scrapy.selector import HtmlXPathSelector

from scrapy.shell import inspect_response

from ..items import FetcherItem
from . import Spider

class FetcherSpider(Spider):
    tag = None


    start_urls = [
        "http://gelbooru.com/index.php?page=post&s=list"
        ]

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        for a in hxs.select("//span[@class='thumb']/a"):
            post_id = cgi.parse_qs(a.select('@href').extract()[0])['id'][0]
            thumb_url = a.select('img/@src').extract()[0]
            image_url = "http://cdn3.gelbooru.com//images/{}/{}".format(*self._parse_thumb_url(thumb_url))
            tags = a.select('img/@alt').extract()[0].strip().split(' ')
            yield FetcherItem(
                image_urls=[image_url],
                tags=tags,
                post_id=post_id
            )


        next_url = hxs.select("//div[@id='paginator']//a[@alt='next']/@href").extract()
        if next_url:
            next_url = self.get_base_url() + next_url[0]
            yield Request(next_url, callback=self.parse)

    def _parse_thumb_url(self, thumb_url):
        return re.findall('^http://cdn\d+.gelbooru.com/thumbs/(\d+)/thumbnail_(.*?)\?\d+$', thumb_url)[0]

    def get_base_url(self):
        parsed_url = urlparse.urlsplit(self.start_urls[0])
        base_url = urlparse.urlunparse([parsed_url.scheme, parsed_url.netloc, parsed_url.path, '', '', ''])
        return base_url

