#     http://doc.scrapy.org/topics/settings.html


HTTPCACHE_DIR = '/media/hd1000/gelbooru/.cache/'
IMAGES_STORE = '/media/hd1000/gelbooru/'
HTTP_PROXY = 'http://127.0.0.1:8118'

TOR_SWITCH_COUNT = 15


BOT_NAME = 'fetcher'
BOT_VERSION = '1.0'

SPIDER_MODULES = ['fetcher.spiders']
NEWSPIDER_MODULE = 'fetcher.spiders'

DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.httpcache.HttpCacheMiddleware': 100,
    'scrapy.contrib.downloadermiddleware.httpcache.DepthMiddleware': None,

    'fetcher.middleware.CustomHttpProxyMiddleware': 300,
    'fetcher.middleware.RandomUserAgentMiddleware': 400,
    'fetcher.middleware.TorSwitchIPMiddleware': 1000,
    }


HTTPCACHE_ENABLED = True
HTTPCACHE_EXPIRATION_SECS = 0


ITEM_PIPELINES = [
    'fetcher.pipelines.FetcherImagePipeline',
]



AUTOTHROTTLE_ENABLED = True
AUTOTHROTTLE_DEBUG = True

CONCURRENT_REQUESTS = 5
CONCURRENT_REQUESTS_PER_DOMAIN = 5

DOWNLOAD_DELAY = 1

