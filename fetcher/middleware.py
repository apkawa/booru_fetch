import socket
import random

from scrapy.contrib.downloadermiddleware.httpproxy import HttpProxyMiddleware
from scrapy import conf
from scrapy.exceptions import NotConfigured
from scrapy import log


TOR_SWITCH_COUNT = conf.settings.getint("TOR_SWITCH_COUNT")

class TorSwitchIPMiddleware(object):
    counter_attr = '__tor_swith_attr'

    def get_count(self, spider):
        count = getattr(spider, self.counter_attr, None)
        if count is None:
            count = TOR_SWITCH_COUNT

        self.set_count(spider, count - 1)
        return count

    def set_count(self, spider, count=TOR_SWITCH_COUNT):
        setattr(spider, self.counter_attr, count)

    def tor_switch_ip(self):
        TOR_IP = '127.0.0.1'
        TOR_PORT = 9051

        AUTH_MSG = "authenticate \n"
        CHANGE_MSG = "signal newnym \n"
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((TOR_IP, TOR_PORT))
        sock.send(AUTH_MSG)
        sock.recv(512)
        sock.send(CHANGE_MSG)
        sock.recv(512)
        sock.close()

    def process_request(self, request, spider):

        count = self.get_count(spider)

        if count <= 0:
            log.msg("Tor switched ip", level=log.DEBUG)
            self.tor_switch_ip()


class CustomHttpProxyMiddleware(HttpProxyMiddleware):
    def __init__(self):
        settings = conf.settings
        self.proxies = {}
        for schema in ['http', 'https']:
            url = settings.get(schema.upper() + '_PROXY')
            if url:
                self.proxies[schema] = self._get_proxy(url, schema)

        if not self.proxies:
            raise NotConfigured


class RandomUserAgentMiddleware(object):

    def get_user_agent(self):
        from uagent_gen import get_user_agent
        return random.choice(get_user_agent())

    def process_request(self, request, spider):
        request.headers.setdefault('User-Agent', self.get_user_agent())